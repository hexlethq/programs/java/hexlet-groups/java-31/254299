package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

class App {
    // BEGIN
    public static String buildList(String[] items) {
        if (items.length == 0) {
            return "";
        }

        StringBuilder list = new StringBuilder("<ul>");

        for (String item : items) {
            String listItem = String.format("\n  <li>%s</li>", item);
            list.append(listItem);
        }

        list.append("\n</ul>");

        return list.toString();
    }

    public static String getUsersByYear(String[][] users, int year) {
        String[] userList = new String[users.length];
        int usersCount = 0;

        for (String[] user : users) {
            int userYear = LocalDate.parse(user[1]).getYear();

            if (userYear == year) {
                userList[usersCount] = user[0];
                usersCount++;
            }
        }

        String[] userListItems = usersCount > 0
                ? Arrays.copyOf(userList, usersCount)
                : new String[0];

        return buildList(userListItems);
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) {
        // BEGIN
        String youngestUser = "";

        if (users.length == 0) {
            return youngestUser;
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
        LocalDate parsedDate = LocalDate.parse(date, formatter);
        LocalDate birthdayOfYoungest = LocalDate.MIN;

        for (String[] user : users) {
            DateTimeFormatter birthdayFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate birthday = LocalDate.parse(user[1], birthdayFormatter);

            if (birthday.isBefore(parsedDate) && birthday.isAfter(birthdayOfYoungest)) {
                birthdayOfYoungest = birthday;
                youngestUser = user[0];
            }
        }

        return youngestUser;
        // END
    }
}
