package exercise;

class Triangle {
    public static double getSquare(int i, int i1, int i2) {
        double angleInRadians = Math.toRadians(i2);

        return i * i1 / 2 * Math.sin(angleInRadians);
    }

    // BEGIN
    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }
    // END
}
