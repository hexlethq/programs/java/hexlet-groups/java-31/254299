package exercise;

class Converter {
    // BEGIN
    public static int convert(int num, String dir) {
        switch (dir) {
            case "b":
                return num * 1024;
            case "Kb":
                return num / 1024;
            default:
                return 0;
        }
    }

    public static void main(String[] args) {
        int kilobytes = 10;
        int bytes = convert(kilobytes, "b");

        System.out.printf("%d Kb = %d b", kilobytes, bytes);
    }
    // END
}
