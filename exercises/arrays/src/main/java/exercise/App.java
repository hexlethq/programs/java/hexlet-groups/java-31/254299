package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] numbers) {
        int[] result = new int[numbers.length];

        for (int i = numbers.length - 1, n = 0; i >= 0; i--, n++) {
            result[n] = numbers[i];
        }

        return result;
    }

    public static int mult(int[] numbers) {
        int result = 1;

        for (int number : numbers) {
            result *= number;
        }

        return result;
    }

    public static int[] flattenMatrix(int[][] matrix) {
        int outerLen = matrix.length;

        if (outerLen == 0) {
            return new int[0];
        }

        int innerLen = matrix[0].length;
        int[] result = new int[outerLen * innerLen];

        for (int i = 0, l = 0; i < outerLen; i++, l += innerLen) {
            int[] numbers = matrix[i];

            for (int n = 0; n < numbers.length; n++) {
                result[l + n] = numbers[n];
            }
        }

        return result;
    }
    // END
}
