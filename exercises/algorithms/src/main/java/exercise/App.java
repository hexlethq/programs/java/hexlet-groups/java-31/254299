package exercise;

class App {
    // BEGIN
    public static int[] sort(int[] numbers) {
        if (numbers.length <= 1) {
            return numbers;
        }

        int changesCount;

        do {
            changesCount = 0;
            for (int i = 1; i < numbers.length; i++) {
                int curr = numbers[i];
                int prev = numbers[i - 1];

                if (curr < prev) {
                    numbers[i - 1] = curr;
                    numbers[i] = prev;
                    changesCount++;
                }
            }
        } while (changesCount != 0);

        return numbers;
    }

    public static int[] selectionSort(int[] numbers) {
        if (numbers.length <= 1) {
            return numbers;
        }

        for (int i = 0; i < numbers.length - 1; i++) {
            int curr = numbers[i];
            int minValue = Integer.MAX_VALUE;
            int minIndex = i;

            for (int n = i; n < numbers.length; n++) {
                if (minValue > numbers[n]) {
                    minValue = numbers[n];
                    minIndex = n;
                }
            }

            if (minIndex != i) {
                numbers[i] = minValue;
                numbers[minIndex] = curr;
            }
        }

        return numbers;
    }
    // END
}
