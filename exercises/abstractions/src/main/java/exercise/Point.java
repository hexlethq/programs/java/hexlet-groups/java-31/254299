package exercise;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y) {
        return new int[]{x, y};
    }

    public static int getX(int[] point) {
        return point[0];
    }

    public static int getY(int[] point) {
        return point[1];
    }

    public static String pointToString(int[] point) {
        return String.format("(%d, %d)", getX(point), getY(point));
    }

    public static int getQuadrant(int[] point) {
        int x = getX(point);
        int y = getY(point);

        if (x == 0 || y == 0) {
            return 0;
        }
        if (x > 0) {
            return y > 0 ? 1 : 4;
        }

        return y > 0 ? 2 : 3;
    }

    public static int[] getSymmetricalPointByX(int[] point) {
        return makePoint(getX(point), -getY(point));
    }

    public static double calculateDistance(int[] point1, int[] point2) {
        int distanceX = Math.abs(getX(point1) - getX(point2));
        int distanceY = Math.abs(getY(point1) - getY(point2));

        return Math.sqrt(Math.pow(distanceX, 2) + Math.pow(distanceY, 2));
    }
    // END
}
