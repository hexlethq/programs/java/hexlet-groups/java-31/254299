package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str) {
        String[] arr = str.toUpperCase().split(" ");
        String result = "";

        for (int i = 0; i < arr.length; i++) {
            String word = arr[i];

            if (word.length() > 0) {
                char firstLetter = word.charAt(0);
                result += firstLetter;
            }
        }

        return result;
    }
    // END
}
