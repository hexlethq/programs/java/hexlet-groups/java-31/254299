package exercise;

class App {
    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable = number % 2 != 0 && number >= 1001;
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        String result = number % 2 == 0 ? "yes" : "no";

        System.out.println(result);
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        if (minutes >= 0 && minutes <= 14) {
            System.out.println("First");
        } else if (minutes >= 15 && minutes <= 30) {
            System.out.println("Second");
        } else if (minutes >= 31 && minutes <= 45) {
            System.out.println("Third");
        } else if (minutes >= 46 && minutes <= 59) {
            System.out.println("Fourth");
        } else {
            System.out.println("Number must be between 0 and 59");
        }
        // END
    }
}
