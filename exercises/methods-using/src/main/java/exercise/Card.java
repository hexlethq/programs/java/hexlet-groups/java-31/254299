package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String placeholder = "*".repeat(starsCount);
        String visibleCardNumber = cardNumber.substring(cardNumber.length() - 4);

        System.out.print(String.format("%s%s", placeholder, visibleCardNumber));
        // END
    }
}
