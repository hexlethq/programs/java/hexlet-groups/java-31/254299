package exercise;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        boolean isExclamatory = sentence.endsWith("!");
        String result = isExclamatory ? sentence.toUpperCase() : sentence.toLowerCase();

        System.out.println(result);
        // END
    }
}
