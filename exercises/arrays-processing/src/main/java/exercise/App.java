package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] numbers) {
        int maxNegative = getMax(getNegatives(numbers));

        return getIndexOfNumber(numbers, maxNegative);
    }

    public static int[] getElementsLessAverage(int[] numbers) {
        int[] result = new int[numbers.length];

        if (numbers.length == 0) {
            return result;
        }

        int avg = getAverage(numbers);
        int lessAvgCount = 0;

        for (int number : numbers) {
            if (number <= avg) {
                result[lessAvgCount] = number;
                lessAvgCount++;
            }
        }

        return Arrays.copyOf(result, lessAvgCount);
    }

    public static int getSumBeforeMinAndMax(int[] numbers) {
        int result = 0;

        if (numbers.length <= 2) {
            return result;
        }

        int max = getMax(numbers);
        int min = getMin(numbers);

        if (max == min) {
            return result;
        }

        int indexOfMax = getIndexOfNumber(numbers, max);
        int indexOfMin = getIndexOfNumber(numbers, min);

        int[] numsBetweenMinAndMax = indexOfMax < indexOfMin
                ? Arrays.copyOfRange(numbers, indexOfMax + 1, indexOfMin)
                : Arrays.copyOfRange(numbers, indexOfMin + 1, indexOfMax);

        for (int number : numsBetweenMinAndMax) {
            result += number;
        }

        return result;
    }

    private static int getAverage(int[] numbers) {
        int sum = 0;

        for (int number : numbers) {
            sum += number;
        }

        return sum / numbers.length;
    }

    private static int getIndexOfNumber(int[] numbers, int number) {
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] == number) {
                return i;
            }
        }

        return -1;
    }

    private static int getMax(int[] numbers) {
        int max = Integer.MIN_VALUE;

        for (int number : numbers) {
            max = Math.max(number, max);
        }

        return max;
    }

    private static int getMin(int[] numbers) {
        int min = Integer.MAX_VALUE;

        for (int number : numbers) {
            min = Math.min(number, min);
        }

        return min;
    }

    private static int[] getNegatives(int[] numbers) {
        int[] result = new int[numbers.length];
        int negativesCount = 0;

        for (int number : numbers) {
            if (number < 0) {
                result[negativesCount] = number;
                negativesCount++;
            }
        }

        return negativesCount > 0 ? Arrays.copyOf(result, negativesCount) : new int[0];
    }
    // END
}
