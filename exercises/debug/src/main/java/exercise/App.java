package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int length1, int length2, int length3) {
        boolean isTriangle = length1 + length2 > length3
                && length2 + length3 > length1
                && length1 + length3 > length2;
        boolean isEquilateral = length1 == length2 && length2 == length3;
        boolean isIsosceles = length1 == length2 || length2 == length3 || length1 == length3;


        if (!isTriangle) {
            return "Треугольник не существует";
        }
        if (isEquilateral) {
            return "Равносторонний";
        }
        if (isIsosceles) {
            return "Равнобедренный";
        }
        return "Разносторонний";
    }

    public static int getFinalGrade(int exam, int project) {
        if (exam < 0 || exam > 100 || project < 0) {
            return -1;
        }
        if (exam > 90 || project > 10) {
            return 100;
        }
        if (exam > 75 && project >= 5) {
            return 90;
        }
        if (exam > 50 && project >= 2) {
            return 75;
        }
        return 0;
    }
    // END
}
