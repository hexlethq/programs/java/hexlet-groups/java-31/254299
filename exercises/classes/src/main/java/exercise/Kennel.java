package exercise;

import java.util.Arrays;


// BEGIN
public class Kennel {
    private static String[][] puppies = new String[][]{};
    private static int puppyCount = 0;

    public static void addPuppy(String[] puppy) {
        puppies = Arrays.copyOf(puppies, puppies.length + 1);
        puppies[puppies.length - 1] = puppy;
        puppyCount++;
    }

    public static void addSomePuppies(String[][] puppiesArr) {
        puppies = Arrays.copyOf(puppies, puppies.length + puppiesArr.length);

        for (String[] puppy : puppiesArr) {
            puppies[puppyCount] = puppy;
            puppyCount++;
        }
    }

    public static int getPuppyCount() {
        return puppyCount;
    }

    public static boolean isContainPuppy(String puppyName) {
        for (String[] puppy : puppies) {
            if (puppy[0].equals(puppyName)) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        return puppies;
    }

    public static String[] getNamesByBreed(String breed) {
        String[] names = new String[puppies.length];
        int namesCount = 0;

        for (String[] puppy : puppies) {
            if (puppy[1].equals(breed)) {
                names[namesCount] = puppy[0];
                namesCount++;
            }
        }

        return namesCount > 0 ? Arrays.copyOf(names, namesCount) : new String[0];
    }

    public static void resetKennel() {
        puppies = new String[][]{};
        puppyCount = 0;
    }

    public static boolean removePuppy(String name) {
        for (int i = 0; i < puppies.length; i++) {
            String[] puppy = puppies[i];

            if (puppy[0].equals(name)) {
                String[][] firstPart = Arrays.copyOfRange(puppies, 0, i);
                String[][] secondPart = Arrays.copyOfRange(puppies, i + 1, puppies.length);

                resetKennel();
                addSomePuppies(firstPart);
                addSomePuppies(secondPart);

                return true;
            }
        }
        return false;
    }
}
// END
